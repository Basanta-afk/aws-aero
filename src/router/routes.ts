// import { RouteConfig } from 'vue-router';
import { auth } from '../firebase/firebase';
import { onAuthStateChanged } from 'firebase/auth';

const routes = () =>
  // store
  {
    return [
      {
        path: '/',
        component: () => import('layouts/MainLayout.vue'),
        children: [{ path: '/', component: () => import('pages/Landing.vue') }],
      },
    ];
  };
// Always leave this as last one,
// but you can also remove it

export default routes;
