// Import the functions you need from the SDKs you need

import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
// export const IS_DEV = process.env.NODE_ENV === 'development';

const firebaseConfig =
  // IS_DEV
  //   ?
  // {
  //     apiKey: 'AIzaSyAIbgXrnq1sk0drsKuhWl5SwDB8oyUgM8o',
  //     authDomain: 'qmed-b3567.firebaseapp.com',
  //     databaseURL:
  //       'https://qmed-b3567-default-rtdb.asia-southeast1.firebasedatabase.app',
  //     projectId: 'qmed-b3567',
  //     storageBucket: 'qmed-b3567.appspot.com',
  //     messagingSenderId: '640173398208',
  //     appId: '1:640173398208:web:64d34a681a13f1be597525',
  //   }
  // :
  {
    apiKey: 'AIzaSyDYNkhMA36oW3c_qsKBNBLNrWHaLtDFAlY',
    authDomain: 'q-med-dev.firebaseapp.com',
    databaseURL:
      'https://q-med-dev-default-rtdb.asia-southeast1.firebasedatabase.app',
    projectId: 'q-med-dev',
    storageBucket: 'q-med-dev.appspot.com',
    messagingSenderId: '237870415173',
    appId: '1:237870415173:web:d2fbd519277812570e922c',
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const fireDatabase = getDatabase(app);

export { auth, app, fireDatabase };

//firebase config
